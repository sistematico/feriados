<?php

$data = file_get_contents('json' . DIRECTORY_SEPARATOR . 'feriados.json');
$datas = json_decode($data, true);
$agora = date("Y-m-d", time());

foreach ($datas as $chave => $dia) {
    if (strtotime($chave)-strtotime($agora) >= 0) {
        $feriados[$chave][] = abs(strtotime($agora) - strtotime($chave));
    }
}

asort($feriados);
$dataFeriado = key($feriados);
$nomeFeriado = $datas[$dataFeriado];

next($feriados);

$dataProximoFeriado = key($feriados);
$nomeProximoFeriado = $datas[$dataProximoFeriado];

$formato = 'Y-m-d';
$date = DateTime::createFromFormat($formato, $dataFeriado);
$dataFeriado = $date->format('d-m-Y');

$date = DateTime::createFromFormat($formato, $dataProximoFeriado);
$dataProximoFeriado = $date->format('d-m-Y');

?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="Feriados">
    <meta name="author" content="Lucas Saliés Brum">
    <title>Feriados - <?=$nomeFeriado?></title>
    <meta property="og:title" content="Feriados" />
    <meta property="og:description" content="Script PHP que mostra o feriado mais próximo com base em um json." />
    <meta property="og:type" content="website" />
    <meta property="og:url" content="https://feriados.lucasbrum.net" />
    <meta property="og:image" content="img/calendar.png" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bulma/0.7.4/css/bulma.min.css">
    <link href="https://fonts.googleapis.com/css?family=Nunito&display=swap" rel="stylesheet">
    <script defer src="https://use.fontawesome.com/releases/v5.3.1/js/all.js"></script>
    <?php if (strtotime($agora) == strtotime($dataFeriado)) { ?>
    <link rel="stylesheet" href="css/bokeh.css">
    <?php } ?>
    <style type="text/css">
        body { font-family: 'Nunito', sans-serif; font-size: 1.2em; }
    </style>
    <link rel="shortcut icon" type="image/x-icon" href="img/favicon.ico">
</head>
<body>
<?php 
if (strtotime($agora) == strtotime($dataFeriado)) { 
    include('bokeh.php');
}
?>
<section class="hero is-success is-fullheight">
    <div class="hero-body">
        <div class="container has-text-centered">
            <?php if (strtotime($agora) == strtotime($dataFeriado)) { ?>
			<figure class="image container is-128x128">
                <img class="icone" src="img/feriado.svg">
			</figure>
            <h1 class="title">
                <?=$nomeFeriado?>
            </h1>
            <h2 class="subtitle">É hoje!</h2>
            <?php } else { ?>
            <figure class="image container is-128x128">
  			    <img class="icone" src="img/poucos.svg">
			</figure>
            <h1 class="title">
                <?=$nomeFeriado?>
            </h1>
            <h2 class="subtitle">Próxima data comemorativa</h2>
            <?php } ?>
            <p><?=$dataFeriado?></p>
            <?php if (strtotime($agora) == strtotime($dataFeriado)) { ?>
            <p class="is-size-8 is-italic">Próximo: <?=$dataProximoFeriado?>: <?=$nomeProximoFeriado?></p>
            <?php } ?>
        </div>
    </div>
</section>
<footer class="footer">
    <div class="content has-text-centered">
        <p>
            Criado com o <strong>Bulma</strong> por <a href="https://lucasbrum.net">Lucas Saliés Brum</a>.
            Código no <a href="https://gitlab.com/sistematico/feriados">Gitlab</a>.
        </p>
    </div>
</footer>
</body>
</html>