<?php

$data = file_get_contents('json' . DIRECTORY_SEPARATOR . 'feriados.json');
$datas = json_decode($data, true);
$agora = date("Y-m-d", time());

foreach ($datas as $chave => $dia) {
    if (strtotime($chave)-strtotime($agora) >= 0) {
        $feriados[$chave][] = abs(strtotime($agora) - strtotime($chave));
    }
}

asort($feriados);
$dataFeriado = key($feriados);
$nomeFeriado = $datas[$dataFeriado];

$formato = 'Y-m-d';
$date = DateTime::createFromFormat($formato, $dataFeriado);
$dataFeriado = $date->format('d-m-Y');

?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="Feriados">
    <meta name="author" content="Lucas Saliés Brum">
    <title>Feriados - <?=$nomeFeriado?></title>
    <meta property="og:title" content="Feriados" />
    <meta property="og:description" content="Script PHP que mostra o feriado mais próximo com base em um json." />
    <meta property="og:type" content="website" />
    <meta property="og:url" content="https://gitlab.com/sistematico/feriados" />
    <meta property="og:image" content="img/calendar.png" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bulma/0.7.4/css/bulma.min.css">
    <link href="https://fonts.googleapis.com/css?family=Nunito&display=swap" rel="stylesheet">
    <script defer src="https://use.fontawesome.com/releases/v5.3.1/js/all.js"></script>
    <style type="text/css">
        body { font-family: 'Nunito', sans-serif; font-size: 1.2em; }
    </style>
    <link rel="shortcut icon" type="image/x-icon" href="img/favicon.ico">
</head>
<body>
<section class="hero is-success is-fullheight">
    <div class="hero-body">
        <div class="container has-text-centered">
            <?php if (strtotime($agora) == strtotime($dataFeriado)) { ?>
            <h1 class="title"><span style='font-size:50px;'>&#127881;</span> <?=$nomeFeriado?></h1>
            <h2 class="subtitle">É hoje!</h2>
            <?php } else { ?>
            <h1 class="title"><span style='font-size:50px;'>&#128512;</span> <?=$nomeFeriado?></h1>
            <h2 class="subtitle">Próxima data comemorativa</h2>
            <?php } ?>
            <p><?=$dataFeriado?></p>
        </div>
    </div>
</section>

<footer class="footer">
    <div class="content has-text-centered">
        <p>
            Criado com o <strong>Bulma</strong> por <a href="https://lucasbrum.net">Lucas Saliés Brum</a>.
            Código no <a href="https://gitlab.com/sistematico/feriados">Gitlab</a>.
        </p>
    </div>
</footer>
</body>
</html>